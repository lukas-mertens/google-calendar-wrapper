# An Unofficial Google Calendar Desktop Application

This application wraps Google Calendar into an electron app. That's it.

## Latest Builds
 - [Linux](https://gitlab.com/lukas-mertens/google-calendar-wrapper/-/jobs/artifacts/main/raw/build/GoogleCalendar-linux-x64.tar.gz?job=build)
 - [Windows](https://gitlab.com/lukas-mertens/google-calendar-wrapper/-/jobs/artifacts/main/raw/build/GoogleCalendar-win32-x64.tar.gz?job=build)
 - [MacOS](https://gitlab.com/lukas-mertens/google-calendar-wrapper/-/jobs/artifacts/main/raw/build/GoogleCalendar-darwin-x64.tar.gz?job=build)
